import pytest

import pycad


def test_flask_init():
    app = pycad.app.test_client()

    response = app.get("/api")

    assert b"Hello, world!" in response.data
