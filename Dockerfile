FROM python:3.6.5-alpine3.7

RUN apk update

RUN apk add git

RUN git clone -b development https://gitlab.com/PyCAD/PyCAD-API.git /var/app

WORKDIR /var/app

RUN pip install -e .

EXPOSE 8000

ENTRYPOINT ["/usr/local/bin/gunicorn", "--config", "gunicorn.conf", "-b", ":8000", "pycad:app"]